public class Media {

	private String author;
	private String title;
	private int nVotes;
	private int notes;

	public Media(String author, String title) {
		this.title = title;
		this.author = author;
		this.notes = 0;
		this.nVotes = 0;
	}

	public String toString() {
		return "\"" + this.title + "\" par " + this.author;
	}

	public double moyenneNotes() {
		if (this.nVotes == 0) {
			return 0.0;
		} else {
			return this.notes / this.nVotes;
		}
	}

	public void vote(int note) {
		if (note < 0 || note > 5) {
			return;
		}
		this.notes += note;
	}

	public String getTitle(){
		return this.title;
	}
	
	
	public String getAuthor() {
		return this.author;
	}

}
