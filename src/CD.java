public class CD extends Media {

	private String type;

	public CD(String author, String title, String type) {
		super(author, title);
		this.type = type;
	}
	
	public CD(String author, String title) {
		this(author, title, "CD musical");
	}

	@Override
	public String toString() {
		return super.toString() + " [" + this.type + "]";
	}

}
