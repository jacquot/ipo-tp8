import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Mediatheque {

	private List<Media> collection;

	public Mediatheque() {
		this.collection = new ArrayList<Media>();
	}

	public List<Media> searchByTitle(String valeur) {
		ArrayList<Media> res = new ArrayList<Media>();

		if (valeur == null) {
			return res;
		}

		for (Media el : this.collection) {
			if (valeur.equalsIgnoreCase(el.getTitle())) {
				res.add(el);
			}
		}

		return res;
	}

	public List<Media> searchByAuthor(String valeur) {
		ArrayList<Media> res = new ArrayList<Media>();

		if (valeur == null) {
			return res;
		}

		for (Media el : this.collection) {
			if (valeur.equalsIgnoreCase(el.getAuthor())) {
				res.add(el);
			}
		}

		return res;
	}

	private List<Media> filterCD() {
		ArrayList<Media> res = new ArrayList<Media>();
		for (Media el : collection) {
			if (el instanceof CD) {
				res.add(el);
			}
		}
		return res;
	}

	private List<Media> filterDVD() {
		ArrayList<Media> res = new ArrayList<Media>();
		for (Media el : collection) {
			if (el instanceof DVD) {
				res.add(el);
			}
		}
		return res;
	}

	private List<Media> filterLivre() {
		ArrayList<Media> res = new ArrayList<Media>();
		for (Media el : collection) {
			if (el instanceof Livre) {
				res.add(el);
			}
		}
		return res;
	}

	public List<Media> filterByMedia(String valeur) {
		ArrayList<Media> res = new ArrayList<Media>();

		if (valeur == null) {
			return res;
		}

		if (valeur.equalsIgnoreCase("CD")) {
			return filterCD();
		}

		if (valeur.equalsIgnoreCase("DVD")) {
			return filterDVD();
		}

		if (valeur.equalsIgnoreCase("Livre")) {
			return filterLivre();
		}

		return res;
	}

	public List<Media> filtre(String critere, String valeur) {
		ArrayList<Media> res = new ArrayList<Media>();

		// facultatif ici : comparaison dans le bon sens avec chaine de
		// caractere fixe
		if (critere == null) {
			return res;
		}

		if ("titre".equalsIgnoreCase(critere)) {
			return searchByTitle(valeur);
		}

		if ("auteur".equalsIgnoreCase(critere)) {
			return searchByAuthor(valeur);
		}

		if ("media".equalsIgnoreCase(critere)) {
			return filterByMedia(valeur);
		}

		return res;
	}

	public List<Media> multiFiltre(Map<String, String> assocCritereValeur) {
		Mediatheque currentSet = this;

		Set<String> criteres = assocCritereValeur.keySet();

		for (String crit : criteres) {
			Mediatheque newSet = new Mediatheque();
			for (Media m : currentSet.filtre(crit, assocCritereValeur.get(crit))) {
				newSet.add(m);
			}
			currentSet = newSet;
		}

		return currentSet.collection;
	}

	public void add(Media el) {
		this.collection.add(el);
	}

	public static void main(String[] a) {
		Mediatheque m = new Mediatheque();
		m.add(new Livre("a", "b"));
		m.add(new CD("titre", "b"));

		for (Media el : m.filtre("media", "livre")) {
			System.out.println(el);
		}

		HashMap<String, String> mesCriteres = new HashMap();
		mesCriteres.put("media", "livre");
		mesCriteres.put("titre", "b");

		List<Media> res = m.multiFiltre(mesCriteres);
		for (Media el : res) {
			System.out.println(el);
		}

	}
}
