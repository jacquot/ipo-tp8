public class DVD extends Media {

	private int zone;

	public DVD(String author, String title, int zone) {
		super(author, title);
		this.zone = zone;
	}

	public boolean readable(int[] zones) {
		if (this.zone == 0) {
			return true;
		}

		for (int z : zones) {
			if (this.zone == z) {
				return true;
			}
		}

		return false;
	}

}
